#ifndef COMPROTOCOL_H
#define COMPROTOCOL_H
#include <QByteArray>
#include <QTime>
#include <QStringList>
namespace comProtocol{
    enum class status : quint8 {
        online, away, busy
    };

    enum class commands : quint8 {
        logIn,
        getUserList,
        getLoginTime,
        getStatus,
        sendMessage,
        getUserAddress,
        requestTime
    };

    QByteArray logIn(const QString& userName);
    QByteArray getUserList(const QStringList& userNames);
    QByteArray getLoginTime(const QString& userName, const QTime& currentTime);
    QByteArray getStatus(const QString& userName, const status userStatus);
    QByteArray sendMessage(const QString& userMessage, const QString& senderName);
    QByteArray getUserAddress(const QString& userName, const QString& userAddress);
    QByteArray requestTime(const QString& userName);



    QString de_logIn(QByteArray const&);
    QStringList de_getUserList(QByteArray const&);
    QPair<QString, QTime> de_getLoginTime(QByteArray const&);
    QPair<QString, status> de_getStatus(QByteArray const&);
    QPair<QString, QString> de_sendMessage(QByteArray const&);
    QPair<QString, QString> de_getUserAddress(QByteArray const&);
}





#endif // COMPROTOCOL_H
