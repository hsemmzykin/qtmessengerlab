 ## DONE LIST:
- Initialize sockets with SSL encryption (since GOST doesn't provide asymmetric algorithms, I'll be using RSA 2048 with OpenSSL) (DONE)
-  Apparently, I have to fix ```EVP_PKEY_get_raw_private_key```, since it doesn't provide interface for 2048-bit RSA. (DONE)
- Create proper window for Client and separate it from server window (DONE)
- Make Image support (DONE)
- Make support for OpenSSL public key reload (DONE)

## TODO List:

- Make XML and INI file support 
