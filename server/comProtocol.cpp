#include "comProtocol.h"


 QByteArray comProtocol::logIn(const QString &userName)
{

    char address = static_cast<char>(comProtocol::commands::logIn);
    QByteArray result(&address, sizeof address);

    result.append(userName.toStdString().c_str());
    return result;
}

QByteArray comProtocol::getUserList(const QStringList& userNames)
{
    char address = static_cast<char>(comProtocol::commands::getUserList);
    QByteArray result(&address, sizeof address);

    for (const QString& username : userNames){
        auto x = username.toStdString().c_str();
        result.append(x);
        result.append('\0');
    }
    return result;
}

 QByteArray comProtocol::getLoginTime(const QString& userName, const QTime &currentTime)
{
    char address = static_cast<char>(comProtocol::commands::getLoginTime);
    QByteArray result(&address, sizeof address);
    result.append(userName.toStdString().c_str());
    result.append('\0');
    QString time = currentTime.toString("hhmmss");
    result.append(time.toStdString().c_str());
    return result;
}

 QByteArray comProtocol::getStatus(const QString& userName, const status userStatus)
{
    char address = static_cast<char>(comProtocol::commands::getStatus);
    QByteArray result(&address, sizeof address);
    result.append(userName.toStdString().c_str());
    result.append('\0');
    char status = static_cast<char>(userStatus);
    result.append(status);
    return result;
}

 QByteArray comProtocol::sendMessage(const QString &userMessage, const QString &senderName)
{
    char address = static_cast<char>(comProtocol::commands::sendMessage);
    QByteArray result(&address, sizeof address);
    result.append(senderName.toStdString().c_str());
    result.append('\0');
    result.append(userMessage.toStdString().c_str());
    return result;
}

 QByteArray comProtocol::getUserAddress(const QString &userName, const QString& userAddress)
{
    char address = static_cast<char>(comProtocol::commands::getUserAddress);
    QByteArray result(&address, sizeof address);
    result.append(userName.toStdString().c_str());
    result.append('\0');
    result.append(userAddress.toStdString().c_str());
    return result;
}

 QByteArray comProtocol::requestTime(const QString &userName)
{

    char address = static_cast<char>(comProtocol::commands::requestTime);
    QByteArray result(&address, sizeof address);

    result.append(userName.toStdString().c_str());
    return result;
}


QString comProtocol::de_logIn(const QByteArray & msg)
{
    QString res;

    if (!msg.length() || static_cast<comProtocol::commands>(msg[0]) != comProtocol::commands::logIn){
        return res;
    }

    return QString(msg.sliced(1));
}

QStringList comProtocol::de_getUserList(const QByteArray & msg)
{
    QStringList res;

    if (!msg.length() || static_cast<comProtocol::commands>(msg[0]) != comProtocol::commands::getUserList){
        return res;
    }
    auto tmp = msg.sliced(1).split('\0');
    for (auto & item : tmp){
        res.push_back(QString(item));
    }
    res.pop_back();
    return res;
}

QPair<QString, QTime> comProtocol::de_getLoginTime(const QByteArray &msg)
{
    QPair<QString, QTime> res;

    if (!msg.length() || static_cast<comProtocol::commands>(msg[0]) != comProtocol::commands::getLoginTime){
        return res;
    }
    auto slashZeroIndex = msg.indexOf('\0', 1);
    if (slashZeroIndex == -1){
        return res;
    }
    res.first = QString(msg.sliced(1, slashZeroIndex).toStdString().c_str());
    res.first = QString(res.first);
    if (slashZeroIndex + 7 != msg.length()){
        return res;
    }
    res.second = QTime(QString(msg.sliced(slashZeroIndex + 1, 2)).toInt(), QString(msg.sliced(slashZeroIndex + 3, 2)).toInt(), QString(msg.sliced(slashZeroIndex + 5, 2)).toInt());
    return res;
}

QPair<QString, comProtocol::status> comProtocol::de_getStatus(const QByteArray & msg)
{
    // THE PROBLEM IS WITH NULL-TERMINATOR AT THE BEGINNING
    QPair<QString, comProtocol::status> res;
    if (!msg.length() || static_cast<comProtocol::commands>(msg[0]) != comProtocol::commands::getStatus){
        return res;
    }
    res.first = QString(msg.sliced(1, msg.indexOf('\0')).toStdString().c_str());

    //SIGSERV after this line
    res.second = static_cast<comProtocol::status>(msg[msg.indexOf('\0') + 1]);
    return res;
}

QPair<QString, QString> comProtocol::de_sendMessage(const QByteArray & msg)
{
    QPair<QString, QString> res;
    if (!msg.length() || static_cast<comProtocol::commands>(msg[0]) != comProtocol::commands::sendMessage){
        return res;
    }
    auto qlist = msg.sliced(1).split('\0');
    res.first = qlist[0];
    res.second = qlist[1];
    return res;
}

QPair<QString, QString> comProtocol::de_getUserAddress(const QByteArray & msg)
{
    QPair<QString, QString> res;
    if (!msg.length() || static_cast<comProtocol::commands>(msg[0]) != comProtocol::commands::getUserAddress){
        return res;
    }

    auto qlist = msg.sliced(1).split('\0');
    res.first = qlist[0];
    res.second = qlist[1];
    return res;
}
QString comProtocol::de_requestTime(const QByteArray & msg)
{
    QString res;

    if (!msg.length() || static_cast<comProtocol::commands>(msg[0]) != comProtocol::commands::requestTime){
        return res;
    }
    return QString(msg.sliced(1));
}
