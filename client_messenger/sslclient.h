#ifndef SSLCLIENT_H
#define SSLCLIENT_H

#include "qpixmap.h"
#include <QObject>
#include <QSslSocket>
#include "../server/comProtocol.h"
class sslclient : public QObject
{
    Q_OBJECT
public:
    QSslSocket * socket = nullptr;
    QString serverIp;
    quint16 serverPort;
    comProtocol::status status;
    QPixmap avatar;
    QString myName;
    QString connectedDate;
    QObject * parent = nullptr;
public:
    explicit sslclient(QObject *parent = nullptr, QString serverIpPort = "127.0.0.1:45678");

public slots:
    void onConnected();

};

#endif // SSLCLIENT_H
