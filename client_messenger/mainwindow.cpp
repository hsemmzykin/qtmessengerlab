#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "sslclient.h"
#include <QDialog>
#include <QPalette>
#include <QLabel>
#include <QMessageBox>
#include <QCloseEvent>
#include <QFile>
#include <QSettings>
#include <QMouseEvent>
#include <QListWidgetItem>
#include <QTime>
#include <QtMultimedia>
#include "../server/comProtocol.h"
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    client = new sslclient(this);
//    restoreSettings();
    ui->userList->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->userList, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(showContextMenu(QPoint)));
    switch(client->status){
        case comProtocol::status::online:
            ui->labelStatus->setText("Online");
            ui->actionOnline->setChecked(true);
            break;
        case comProtocol::status::away:
            ui->labelStatus->setText("Away");
            ui->actionNot_here->setChecked(true);
            break;
        case comProtocol::status::busy:
            ui->labelStatus->setText("Busy...");
            ui->actionDo_not_disturb->setChecked(true);
            break;
        }

}

MainWindow::~MainWindow()
{
//    storeSettings();
    delete ui;
}

// \brief: send message
void MainWindow::on_sendButton_clicked()
{
    ui->messageBox->setText(ui->sendMessage->text());

}


void MainWindow::on_actionConnect_to_server_triggered()
{

}


void MainWindow::on_actionAbout_triggered()
{
    QDialog* infoAboutAuthor = new QDialog(this);
    infoAboutAuthor->setModal(true);
    infoAboutAuthor->setWindowTitle("About");
    QPalette palette = infoAboutAuthor->palette();
    palette.setColor(infoAboutAuthor->backgroundRole(), Qt::green);
    infoAboutAuthor->setPalette(palette);
    QGridLayout *gridLayout = new QGridLayout(infoAboutAuthor);
    QPixmap photo;
    photo.load("/home/sasijeppy/Downloads/icon.jpg");
    QLabel *photoLabel = new QLabel(infoAboutAuthor);
    photoLabel->setPixmap(photo.scaled(300, 300, Qt::KeepAspectRatio));
    gridLayout->addWidget(photoLabel, 0, 0, 16, 1, Qt::AlignBottom);
    QLabel *authorLabel = new QLabel("Maksim Kainitoff (фото 5-летней давности)", infoAboutAuthor);
    gridLayout->addWidget(authorLabel, 4, 1);
    QLabel *buildLabel = new QLabel(QString("Build QT version ") + QT_VERSION_STR, infoAboutAuthor);
    gridLayout->addWidget(buildLabel, 5, 1);
    QLabel *dateLabel = new QLabel(QString("DATE: ") + __DATE__ + ' ' + __TIME__, infoAboutAuthor);
    gridLayout->addWidget(dateLabel, 6, 1);
    QLabel *qtlabel = new QLabel(QString("Current Qt Version: ") + qVersion(), infoAboutAuthor);
    gridLayout->addWidget(qtlabel, 7, 1);

    QPushButton * close  = new QPushButton("Kill window", infoAboutAuthor);
    QObject::connect(close, &QPushButton::clicked, this, [infoAboutAuthor]()->void{infoAboutAuthor->close();});

    infoAboutAuthor->setLayout(gridLayout);
    infoAboutAuthor->show();
}





void MainWindow::on_actionDisconnect_triggered()
{

}


void MainWindow::on_actionSave_to_XML_triggered()
{

}


void MainWindow::on_actionChange_IP_PORT_triggered()
{

}


void MainWindow::on_actionChange_username_triggered()
{

}


void MainWindow::on_actionOnline_triggered()
{
    if (ui->actionDo_not_disturb->isChecked()) {
            ui->actionDo_not_disturb->setChecked(false);
            ui->actionNot_here->setChecked(false);
            client->status = comProtocol::status::online;
            ui->labelStatus->setText("On the line");
        }
}


void MainWindow::on_actionDo_not_disturb_triggered()
{
    if (ui->actionDo_not_disturb->isChecked()) {
            ui->actionOnline->setChecked(false);
            ui->actionNot_here->setChecked(false);
            client->status = comProtocol::status::busy;
            ui->labelStatus->setText("Busy...");
        }
}


void MainWindow::on_actionNot_here_triggered()
{

}


void MainWindow::on_actionExit_triggered()
{
//    this->storeSettings();
    this->close();
}



