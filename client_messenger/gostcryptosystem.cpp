#include "gostcryptosystem.h"
#include "qdebug.h"
#include <QCryptographicHash>

GOSTCryptoSystem::GOSTCryptoSystem()
{
    OPENSSL_add_all_algorithms_conf();
    ERR_load_ERR_strings();

    this->engine_gost = ENGINE_by_id("gost");
    this->cipher_gost = EVP_get_cipherbyname("gost89");

    if( EVP_CIPHER_mode(cipher_gost) == EVP_CIPH_CFB_MODE )
    {
        qDebug() << "Using GOST89 in CFB mode";
    }
    this->ctx = EVP_CIPHER_CTX_new();

    this->iv  = (unsigned char*)"0123456789012345" ;

}
GOSTCryptoSystem::~GOSTCryptoSystem(){
    EVP_CIPHER_CTX_free(ctx);
}
QString GOSTCryptoSystem::encryptText(QString password, QString text)
{
    QCryptographicHash hasher(QCryptographicHash::Algorithm::Sha256);
    QByteArray h_password = hasher.hash(QByteArray(password.toStdString().c_str()), QCryptographicHash::Algorithm::Sha256);
    int init = EVP_EncryptInit_ex( ctx, this->cipher_gost, this->engine_gost, reinterpret_cast<const unsigned char*>(h_password.data()), iv );
    if( !init )
    {
        char err[512];
        ERR_error_string( ERR_get_error(), err );
        qDebug() << "ERROR: " << err;
    }

    int ciphertext_len = 0;
    unsigned char* ciphertext = new unsigned char[text.size() * 2];
    int enc = EVP_EncryptUpdate( ctx, ciphertext, &ciphertext_len, reinterpret_cast<const unsigned char*>(text.toStdString().c_str()), text.size());
    if( !enc )
    {
        char err[512];
        ERR_error_string( ERR_get_error(), err );
        qDebug() << "ERROR: " << err;
    }
    ciphertext[ciphertext_len] = '\0';
    return QString(reinterpret_cast<const char*>(ciphertext));
}

QString GOSTCryptoSystem::decryptText(QString password, QString encryptedText)
{
    int init = EVP_DecryptInit_ex( ctx, cipher_gost, engine_gost, reinterpret_cast<const unsigned char*>(password.toStdString().c_str()), iv );
    if( !init )
    {
        char err[512];
        ERR_error_string( ERR_get_error(), err );
        qDebug() << "ERROR: " << err;
    }

    int text_len = 0;
    unsigned char* text = new unsigned char[encryptedText.size() + 1];
    int enc = EVP_DecryptUpdate( ctx, text, &text_len, reinterpret_cast<const unsigned char*>(encryptedText.toStdString().c_str()), encryptedText.size());
    if( !enc )
    {
        char err[512];
        ERR_error_string( ERR_get_error(), err );
        qDebug() << "ERROR: " << err;
    }
    text[text_len] = '\0';
    return QString(reinterpret_cast<const char*>(text));
}
