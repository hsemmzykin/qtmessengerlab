#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "sslclient.h"
#include <QMediaPlayer>
#include <QCloseEvent>
#include <QDialog>
#include <QPalette>
#include <QLabel>
#include <QMessageBox>
#include <QFile>
#include <QSettings>
#include <QMouseEvent>
#include <QListWidgetItem>
#include <QTime>
#include <QDomDocument>
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void restoreSettings();
    void storeSettings();



private slots:
    void on_sendButton_clicked();
    void on_actionAbout_triggered();
    void on_actionDisconnect_triggered();
    void on_actionSave_to_XML_triggered();
    void on_actionChange_IP_PORT_triggered();
    void on_actionChange_username_triggered();
    void on_actionOnline_triggered();
    void on_actionDo_not_disturb_triggered();
    void on_actionNot_here_triggered();

    void on_actionExit_triggered();

    void on_actionConnect_to_server_triggered();

private:
    Ui::MainWindow *ui;
    sslclient* client = nullptr;
    QVector<sslclient*> clientVector;
    QVector<QString> XML;
    QFile xmlFile;
    QDomDocument generalTag;
    QDomDocument documentTag;
    QDomDocument dateTag;
    QDomDocument timeTag;
    QDomDocument addressTag;
    QDomDocument userNameTag;
    QDomDocument messageInfoTag;


    QSettings * ini;
    QMenu * multiButton;
    QAction* sendPlainText;
    QAction* sendImageData;

protected:
    //    void mousePressEvent(QMouseEvent* parent = nullptr);
};
#endif // MAINWINDOW_H
