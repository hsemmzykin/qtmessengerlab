#ifndef GOSTCRYPTOSYSTEM_H
#define GOSTCRYPTOSYSTEM_H
#include <QString>
#include <openssl/conf.h>
#include <openssl/err.h>
#include <openssl/engine.h>
#include <openssl/evp.h>
class GOSTCryptoSystem
{
    ENGINE *engine_gost;
    const EVP_CIPHER *cipher_gost;
    EVP_CIPHER_CTX *ctx;
    unsigned char *iv;

public:
    GOSTCryptoSystem();
    ~GOSTCryptoSystem();
    QString encryptText(QString password, QString text);
    QString decryptText(QString password, QString encryptedText);
};

#endif // GOSTCRYPTOSYSTEM_H
